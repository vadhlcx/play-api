import {
  createBrowserRouter,
  Route,
  createRoutesFromElements,
  RouterProvider,
  BrowserRouter,
} from "react-router-dom";
import Home from "./Page/Home";
import Contact from "./Page/Contact";
import Product, { productLoader } from "./Page/Product/ListProduct";
import ListProduct from "./Page/Product/ListProduct";
import Header from "./Page/Layout/Header";
import { useAuth } from "./Page/Log/Component/Auth";
import Login from "./Page/Log/Login";
import { DetailProduct, detailLoader } from "./Page/Product/DetailProduct";
import ProductLayout from "./Page/Layout/ProductLayout";

function App() {
  const { auth } = useAuth();
  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route path="/" element={<Header />}>
        <Route index element={<Home />} />
        <Route path="contact" element={<Contact />} />
        <Route path="product" element={<ProductLayout />}>
          <Route index element={<ListProduct />} loader={productLoader} />
          <Route path=":id" element={<DetailProduct />} loader={detailLoader} />
        </Route>
      </Route>
    )
  );

  return <div>{auth ? <RouterProvider router={router} /> : <Login />}</div>;
  // if (auth === null) {
  //   return <Login />;
  // }
  // if (!auth) {
  //   return <BrowserRouter router={router} />;
  // }
}

export default App;
