export const getAllCategories = async () => {
  const resp = await fetch("https://dummyjson.com/products/categories");
  return resp.json();
};
export const AllCategories = await getAllCategories();
