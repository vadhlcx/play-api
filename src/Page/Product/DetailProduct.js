import { useLoaderData, useParams } from "react-router";

export const DetailProduct = () => {
  const { id } = useParams();
  const product = useLoaderData();
  const { title, thumbnail, description, price, rating } = product;
  return (
    <div
      className="d-flex w-75 m-auto flex-sm-column flex-wrap"
      style={{ background: "#82ABA1" }}
    >
      <div className="w-100 p-3 d-flex justify-content-center">
        <img className="img-fluid" src={thumbnail} alt="" />
      </div>
      <div className="text-center product-detail-description d-flex row justify-content-center p-3">
        <h2>{title}</h2>
        <p>{description}</p>
        <p>${price}.00</p>
        <p>Rating : {rating} ⭐</p>
        <button>Buy Now</button>
      </div>
    </div>
  );
};
/**Product Loader */
export const detailLoader = async ({ params }) => {
  const { id } = params;
  const resp = await fetch("https://dummyjson.com/products/" + id);

  return resp.json();
};
