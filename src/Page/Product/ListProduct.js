import { useLoaderData } from "react-router";
import axios from "axios";
import { Link, NavLink } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";
import DetailProduct from "./DetailProduct";
import { useState } from "react";
import { AllCategories } from "./SortProduct";
export default function ListProduct() {
  const productData = useLoaderData();
  const products = productData.products;
  const [search, setSearch] = useState("");
  const [newProduct, setNewProduct] = useState(null);

  const onHandleSubmit = (e) => {
    e.preventDefault();
    const searchText = search;
    const newProductTemp = products.filter((product) => {
      const { title } = product;
      return title.toLowerCase().includes(searchText.toLowerCase());
    });
    setNewProduct(newProductTemp);
  };

  return (
    <div
      className="d-flex row justify-content-center "
      style={{ background: "#fff" }}
    >
      <form className="d-flex justify-content-center" onSubmit={onHandleSubmit}>
        <input
          type="text"
          placeholder="Product Name"
          className="border rounded p-1"
          onChange={(e) => {
            e.preventDefault();
            setSearch(e.target.value);
            if (e.target.value === "") {
              setNewProduct(null);
            }
          }}
        />
        <button className="btn-search-product">Search</button>
      </form>
      <div>
        {newProduct ? (
          <div className="d-flex flex-wrap">
            {newProduct.map((product) => {
              const { id, title, thumbnail, description, price, brand } =
                product;
              return (
                <Link
                  key={id}
                  to={id.toString()}
                  style={{ textDecoration: "none", color: "#000" }}
                  className="col-lg-3 col-md-4 col-sm-6 product-card d-flex row pt-3 pb-2 "
                >
                  <div className="h-75 d-flex justify-content-center p-1">
                    <img className="img-fluid rounded" src={thumbnail} alt="" />
                  </div>
                  <div className=" w-100 d-flex justify-content-start row p-2">
                    <h4 className="title-product">{title}</h4>
                    <p>${price}.00</p>
                    <p style={{ fontWeight: "bold" }}>{brand}</p>
                  </div>
                </Link>
              );
            })}
          </div>
        ) : (
          <div className="d-flex flex-wrap">
            {products.map((product) => {
              const { id, title, thumbnail, description, price, brand } =
                product;
              return (
                <Link
                  key={id}
                  to={id.toString()}
                  style={{ textDecoration: "none", color: "#000" }}
                  className="col-lg-3 col-md-4 col-sm-6 product-card d-flex row pt-3 pb-2 "
                >
                  <div className="h-75 d-flex justify-content-center p-1">
                    <img className="img-fluid rounded" src={thumbnail} alt="" />
                  </div>
                  <div className=" w-100 d-flex justify-content-start row p-2">
                    <h4 className="title-product">{title}</h4>
                    <p>${price}.00</p>
                    <p style={{ fontWeight: "bold" }}>{brand}</p>
                  </div>
                </Link>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
}

export const productLoader = async () => {
  const res = await fetch("https://dummyjson.com/products");
  return res.json();
};
