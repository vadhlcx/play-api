import { createContext, useContext, useState } from "react";
import * as authHelper from "./AuthHelper";

//* context value of auth (token), save token func, logOut (remove token), user, setuser function.
const AuthContext = createContext({
  auth: null,
  saveAuth: function () {},
  logOut: function () {},
  user: null,
  setUser: function () {},
});

const useAuth = () => useContext(AuthContext);

const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState(authHelper.getAuth());
  const [user, setUser] = useState();

  const saveAuth = (auth) => {
    setAuth(auth);
    if (auth) {
      authHelper.setAuth(auth);
    } else {
      authHelper.removeAuth();
    }
  };
  const logOut = () => {
    setAuth(null);
    authHelper.removeAuth();
  };
  return (
    <AuthContext.Provider value={{ auth, saveAuth, logOut, user, setUser }}>
      {children}
    </AuthContext.Provider>
  );
};

export { AuthProvider, useAuth };
