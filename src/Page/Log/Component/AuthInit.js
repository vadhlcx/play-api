import { useEffect } from "react";
import { useAuth } from "./Auth";
import { getUser } from "../request";

const AuthInit = ({ children }) => {
  const { auth, setUser, logOut } = useAuth();

  useEffect(() => {
    const getCurrentUser = () => {
      getUser()
        .then((response) => {
          setUser(response.data);
        })
        .catch(() => {
          logOut();
        });
    };
    getCurrentUser();
    // eslint-disable-next-line
  }, [auth]);
  return children;
};

export { AuthInit };
