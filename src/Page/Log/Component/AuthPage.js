import { Outlet } from "react-router-dom";

function AuthPage(params) {
  return (
    <div>
      <Outlet />
    </div>
  );
}

export default AuthPage;
