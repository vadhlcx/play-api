import { useState } from "react";
import { useAuth } from "./Component/Auth";
import { login } from "./request";
export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const { saveAuth } = useAuth();

  const onLogin = (event) => {
    event.preventDefault();
    login(username, password)
      .then((response) => {
        setError("");
        saveAuth(response.data.token);
      })
      .catch((error) => {
        setError(error.response.data.message);
      });
  };
  return (
    <div className="form-container bg-body-tertiary p-2 w-50 justify-content-center d-flex">
      <form className="d-flex row justify-content-center" onSubmit={onLogin}>
        <h3 className="text-center">KiloIT Login</h3>
        <div className="d-flex row p-2">
          <input
            type="text"
            placeholder="username"
            className="mb-3 rounded"
            onChange={(e) => {
              e.preventDefault();
              setUsername(e.target.value);
            }}
          />
          <input
            type="password"
            placeholder="password"
            className="rounded"
            onChange={(e) => {
              e.preventDefault();
              setPassword(e.target.value);
            }}
          />
        </div>
        <button className="text-center w-25 btn-login-form">Log In</button>
        <p>{error}</p>
      </form>
    </div>
  );
}
