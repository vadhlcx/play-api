import { NavLink, Outlet } from "react-router-dom";
import { useAuth } from "../Log/Component/Auth";
function Header() {
  const { logOut } = useAuth();
  return (
    <>
      <header className="app-header">
        <nav className="d-flex justify-content-around text-center">
          <h1 className="page-name">KiloIT</h1>
          <NavLink to="/">Home</NavLink>
          <NavLink to="contact">Contact</NavLink>
          <NavLink to="product">Product</NavLink>

          {/**  nav link give us a class attribute where we can style it than Nav */}
          <button
            onClick={() => {
              logOut();
            }}
          >
            Log Out
          </button>
        </nav>
      </header>

      <main>
        <Outlet />
      </main>
    </>
  );
}

export default Header;
