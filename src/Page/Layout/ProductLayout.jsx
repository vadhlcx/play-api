import { Outlet } from "react-router";
import { useState } from "react";
import { AllCategories } from "../Product/SortProduct";
import { Link } from "react-router-dom";
export default function ProductLayout() {
  return (
    <div>
      <h3 className="text-center bg-black p-2" style={{ color: "white" }}>
        Our Product
      </h3>
      <main>
        <Outlet></Outlet>
      </main>
    </div>
  );
}
